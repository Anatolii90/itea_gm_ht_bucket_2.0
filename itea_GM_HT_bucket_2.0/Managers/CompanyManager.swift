//
//  CompanyManager.swift
//  itea_GM_HT_bucket_2.0
//
//  Created by Anatolii on 7/2/19.
//  Copyright © 2019 Anatolii. All rights reserved.
//

import UIKit

class CompanyManager: NSObject {
    func createITCompany() -> [ITCompany] {
        
        let cityKiev = City()
        cityKiev.name = "Kiev"
        cityKiev.photo = "kiev1"
        cityKiev.latitude = 50.45466
        cityKiev.longitude = 30.5238
        cityKiev.zoom = 11
        
        let cityDnepr = City()
        cityDnepr.name = "Dnepr"
        cityDnepr.photo = "dnepr"
        cityDnepr.latitude = 48.4624412
        cityDnepr.longitude = 34.8602718
        cityDnepr.zoom = 11
        
        let cityLviv = City()
        cityLviv.name = "Lviv"
        cityLviv.photo = "lviv"
        cityLviv.latitude = 49.8327787
        cityLviv.longitude = 23.9421955
        cityLviv.zoom = 11
        
        let cityKhmel = City()
        cityKhmel.name = "khmelnitskiy"
        cityKhmel.photo = "khmel"
        cityKhmel.latitude = 49.4106425
        cityKhmel.longitude = 26.9252185
        cityKhmel.zoom = 11
        
        let cityOdessa = City()
        cityOdessa.name = "Odessa"
        cityOdessa.photo = "odessa"
        cityOdessa.latitude = 46.460123
        cityOdessa.longitude = 30.5717025
        cityOdessa.zoom = 11
        
        var companyArray: [ITCompany] = []
        let company1 = ITCompany()
        company1.name = "Luxoft"
        company1.logo = "lux"
        company1.adress = "вулиця Радищева, 10/14, Київ, 02000"
        company1.telephone = "+38(044) 238 8108"
        company1.email = "luxoft_hr@gmail.com"
        company1.city = cityKiev
        company1.latitide = 50.4496893
        company1.longitude = 30.4105469
        company1.website = "https://www.luxoft.com/"
        
        let company2 = ITCompany()
        company2.name = "DataArt"
        company2.logo = "art"
        company2.adress = "вулиця Смаль-Стоцького, 1, Львів, Львівська область, 79000"
        company2.telephone = "+38(050) 371 0507"
        company2.email = "hire-dataart@gmail.com"
        company2.city = cityLviv
        company2.latitide = 49.842117
        company2.longitude = 23.9828938
        company2.website = "https://dataart.ua/"
        
        let company3 = ITCompany()
        company3.name = "Ciklum"
        company3.logo = "ciklum"
        company3.adress = "вул. Барикадна, 15А, корп. 3, БЦ Ступени, Дніпро́,  Днепр область, 49000"
        company3.telephone = "+38(056) 375 7198"
        company3.email = "gettociklum@gmail.com"
        company3.city = cityDnepr
        company3.latitide = 48.4622768
        company3.longitude = 35.0490173
        company3.website = "https://www.ciklum.com/"
        
        companyArray.append(company1)
        companyArray.append(company2)
        companyArray.append(company3)
        
        let companyFilial11 = ITCompany()
        companyFilial11.name = "Luxoft Kiev Levui Bereg"
        companyFilial11.logo = "lux"
        companyFilial11.adress = "вулиця Михайла Драгоманова, 14, Київ, 02000"
        companyFilial11.telephone = "+38(044) 238 0001"
        companyFilial11.email = "luxoft_hr@gmail.com"
        companyFilial11.city = cityKiev
        companyFilial11.latitide = 50.4121055
        companyFilial11.longitude = 30.6362149
        companyFilial11.website = "https://www.luxoft.com/"
        
        let companyFilial12 = ITCompany()
        companyFilial12.name = "Luxoft Kiev Pravui Bereg"
        companyFilial12.logo = "lux"
        companyFilial12.adress = "вулиця Михайла Ломоносова, 26, Київ, 03022"
        companyFilial12.telephone = "+38(044) 238 0002"
        companyFilial12.email = "luxoft_hr@gmail.com"
        companyFilial12.city = cityKiev
        companyFilial12.latitide = 50.3914297
        companyFilial12.longitude = 30.4873528
        companyFilial12.website = " https://www.luxoft.com/"
        
        let companyFilial21 = ITCompany()
        companyFilial21.name = "DataArt Kiev"
        companyFilial21.logo = "art"
        companyFilial21.adress = "Бехтеревський провулок, 14, Київ, 04053"
        companyFilial21.telephone = "+38(044) 593 9768"
        companyFilial21.email = "hire-dataart@gmail.com"
        companyFilial21.city = cityKiev
        companyFilial21.latitide = 50.4672754
        companyFilial21.longitude = 30.49146
        companyFilial21.website = "https://dataart.ua/"
        
        let companyFilial22 = ITCompany()
        companyFilial22.name = "DataArt Odessa"
        companyFilial22.logo = "art"
        companyFilial22.adress = "Болгарская ул., 78, Одесса"
        companyFilial22.telephone = "+38(044) 593 0101"
        companyFilial22.email = "hire-dataart@gmail.com"
        companyFilial22.city = cityOdessa
        companyFilial22.latitide = 46.4620203
        companyFilial22.longitude = 30.7158998
        companyFilial22.website = "https://dataart.ua/"
        
        let companyFilial23 = ITCompany()
        companyFilial23.name = "DataArt Odessa academy"
        companyFilial23.logo = "art"
        companyFilial23.adress = "вулиця Велика Арнаутська, 2А, Одеса, Одеська, 65000"
        companyFilial23.telephone = "+38(044) 593 1001"
        companyFilial23.email = "acad-dataart@gmail.com"
        companyFilial23.city = cityOdessa
        companyFilial23.latitide = 46.47152983
        companyFilial23.longitude = 30.7525793
        companyFilial23.website = "https://dataart.ua/"
        
        let companyFilial31 = ITCompany()
        companyFilial31.name = "Ciklum Kiev"
        companyFilial31.logo = "ciklum"
        companyFilial31.adress = "вулиця Миколи Амосова, 12, Київ, 03141"
        companyFilial31.telephone = "+38(093) 123-55-55"
        companyFilial31.email = "gettociklum@gmail.com"
        companyFilial31.city = cityKiev
        companyFilial31.latitide = 50.4326146
        companyFilial31.longitude = 30.5035596
        companyFilial31.website = "https://www.ciklum.com/"
        
        let companyFilial32 = ITCompany()
        companyFilial32.name = "Ciklum Khmelnitskiy"
        companyFilial32.logo = "ciklum"
        companyFilial32.adress = "вулиця Свободи, 37, Хмельницький, Хмельницька область, 29000"
        companyFilial32.telephone = "+38(093) 123-6666"
        companyFilial32.email = "gettociklum@gmail.com"
        companyFilial32.city = cityKhmel
        companyFilial32.latitide = 49.422952
        companyFilial32.longitude = 26.990159
        companyFilial32.website = "https://www.ciklum.com/"
        
        company1.filialsArray.append(companyFilial11)
        company1.filialsArray.append(companyFilial12)
        company2.filialsArray.append(companyFilial21)
        company2.filialsArray.append(companyFilial22)
        company2.filialsArray.append(companyFilial23)
        company3.filialsArray.append(companyFilial31)
        company3.filialsArray.append(companyFilial32)
        
        return companyArray
    }
}
