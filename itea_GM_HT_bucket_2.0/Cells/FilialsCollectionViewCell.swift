//
//  FilialsCollectionViewCell.swift
//  itea_GM_HT_bucket_2.0
//
//  Created by Anatolii on 7/3/19.
//  Copyright © 2019 Anatolii. All rights reserved.
//

import UIKit

class FilialsCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var cityNameLabel: UILabel!
    

    func update(city: City?) {
        imageView.image = UIImage(named: city?.photo ?? "default")
        cityNameLabel.text = city?.name
    }
}
