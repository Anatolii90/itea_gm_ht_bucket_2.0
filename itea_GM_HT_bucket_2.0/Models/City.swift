//
//  City.swift
//  itea_GM_HT_bucket_2.0
//
//  Created by Anatolii on 7/2/19.
//  Copyright © 2019 Anatolii. All rights reserved.
//

import UIKit

class City: NSObject {
    var name = ""
    var photo = ""
    var longitude = 0.0
    var latitude = 0.0
    var zoom = 0.0
}
