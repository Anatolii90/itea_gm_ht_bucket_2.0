//
//  ITCompany.swift
//  itea_GM_HT_bucket_2.0
//
//  Created by Anatolii on 7/2/19.
//  Copyright © 2019 Anatolii. All rights reserved.
//

import UIKit

class ITCompany: NSObject {    
    var name = ""
    var logo = ""
    var adress = ""
    var telephone = ""
    var email = ""
    var website = ""
    var city: City?
    var latitide = 0.0
    var longitude = 0.0
    var filialsArray: [ITCompany] = []
}
