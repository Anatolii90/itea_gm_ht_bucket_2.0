//
//  FilialsViewController.swift
//  itea_GM_HT_bucket_2.0
//
//  Created by Anatolii on 7/3/19.
//  Copyright © 2019 Anatolii. All rights reserved.
//

import UIKit

class FilialsViewController: UIViewController {
    
    @IBOutlet weak var filialsCollectionView: UICollectionView!
    
    var citiesArray: [City] = []
    var currentCompany: ITCompany?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        filialsCollectionView.delegate = self
        filialsCollectionView.dataSource = self
        let xib = UINib(nibName: "FilialsCollectionViewCell", bundle: nil)
        filialsCollectionView.register(xib, forCellWithReuseIdentifier: "FilialsCollectionViewCell")

    }
    
    func update(cities: [City]?, company: ITCompany?) {
        citiesArray = cities ?? []
        currentCompany = company
    }
    
}

extension FilialsViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return citiesArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let item = filialsCollectionView.dequeueReusableCell(withReuseIdentifier: "FilialsCollectionViewCell", for: indexPath) as! FilialsCollectionViewCell
        item.update(city: citiesArray[indexPath.row])
        return item
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: filialsCollectionView.frame.width - 10 , height: 200)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "MapViewController") as! MapViewController
        
        let filialsClouser: (ITCompany) -> [ITCompany] = { (company) in
            return company.filialsArray.filter({$0.city == self.citiesArray[indexPath.row]})
        }
        
        vc.update(currentCity: citiesArray[indexPath.row], filialsArray: filialsClouser(currentCompany ?? ITCompany()))
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}
