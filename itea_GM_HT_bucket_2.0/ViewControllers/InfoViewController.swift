//
//  InfoViewController.swift
//  itea_GM_HT_bucket_2.0
//
//  Created by Anatolii on 7/2/19.
//  Copyright © 2019 Anatolii. All rights reserved.
//

import UIKit
import MessageUI

class InfoViewController: UIViewController {
    
    @IBOutlet weak var photoView: UIView!
    @IBOutlet weak var photoImageView: UIImageView!
    @IBOutlet weak var emailButton: UIButton!
    @IBOutlet weak var companyNameLabel: UILabel!
    @IBOutlet weak var adressLabel: UILabel!
    @IBOutlet weak var telephoneButton: UIButton!
    @IBOutlet weak var websiteButton: UIButton!
    
    var currentCompany: ITCompany?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        photoView.layer.cornerRadius = photoView.frame.width / 2
        fillData()
    }
    
    func update(company: ITCompany?) {
        currentCompany = company
    }
    
    func fillData() {
        photoImageView.image = UIImage(named: currentCompany?.logo ?? "default")
        companyNameLabel.text = currentCompany?.name
        adressLabel.text = currentCompany?.adress
        
        emailButton.setTitle(currentCompany?.email, for: .normal)
        let underlineAttribute = [NSAttributedString.Key.underlineStyle: NSUnderlineStyle.single.rawValue]
        emailButton.titleLabel?.attributedText = NSAttributedString(string: currentCompany?.email ?? "e-mail", attributes: underlineAttribute)
        
        telephoneButton.setTitle(currentCompany?.telephone, for: .normal)
        telephoneButton.titleLabel?.attributedText = NSAttributedString(string: currentCompany?.telephone ?? "telephone", attributes: underlineAttribute)
        
        websiteButton.setTitle(currentCompany?.website, for: .normal)
        websiteButton.titleLabel?.attributedText = NSAttributedString(string: currentCompany?.website ?? "web-site", attributes: underlineAttribute)
    }
    
    @IBAction func emailButtonPressed(_ sender: Any) {
        sendEmail(email: currentCompany?.email ?? "")
    }
    
    @IBAction func showFilialsButtonPressed(_ sender: Any) {
        guard let company = currentCompany else {
            return
        }
        var filialsCityArray: [City] = []
        for filial in company.filialsArray {
            if let currCity = filial.city {
                if !filialsCityArray.contains(currCity) {
                    filialsCityArray.append(currCity)
                }
            }
        }
        if filialsCityArray.count > 1 {
            let vc = storyboard?.instantiateViewController(withIdentifier: "FilialsViewController") as! FilialsViewController
            vc.update(cities: filialsCityArray, company: currentCompany)
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else {
            let vc = storyboard?.instantiateViewController(withIdentifier: "MapViewController") as! MapViewController
            vc.update(currentCity: filialsCityArray.first, filialsArray: currentCompany?.filialsArray ?? [])
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
    }
    
    @IBAction func telephoneButtonPressed(_ sender: Any) {
        sendMessage(number: currentCompany?.telephone ?? "")
    }
    
    @IBAction func websiteButtonPressed(_ sender: Any) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "WebViewController") as! WebViewController
        vc.update(url: currentCompany?.website)
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

extension InfoViewController: MFMessageComposeViewControllerDelegate {
    
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        controller.dismiss(animated: true, completion: nil)
    }
    
    
    func sendMessage(number: String) {
        let messageCompose = MFMessageComposeViewController()
        messageCompose.messageComposeDelegate = self
        messageCompose.recipients = [number]
        messageCompose.body = "Возьмите меня на работу пажжаааалуууста"
        messageCompose.subject = "Работаю за еду"
        present(messageCompose, animated: true, completion: nil)
    }
    
}

extension InfoViewController: MFMailComposeViewControllerDelegate {
    
    func sendEmail(email: String) {
        let composeVC = MFMailComposeViewController()
        composeVC.mailComposeDelegate = self
        composeVC.setToRecipients([email])
        composeVC.setSubject("Hello! Wanna work hard!!")
        composeVC.setMessageBody("I want to work, please call me back!!", isHTML: false)
        present(composeVC, animated: true, completion: nil)
    }
    
}
