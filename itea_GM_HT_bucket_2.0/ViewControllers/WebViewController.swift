//
//  WebViewController.swift
//  itea_GM_HT_bucket_2.0
//
//  Created by Anatolii on 7/3/19.
//  Copyright © 2019 Anatolii. All rights reserved.
//

import UIKit
import Foundation
import WebKit

class WebViewController: UIViewController {
    
    @IBOutlet weak var webView: WKWebView!
    
    var currentURL: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadWebSite()
    }
    
    func loadWebSite() {
        guard let urlString = currentURL else {
            return
        }
        guard let url = URL(string: urlString) else {
            return
        }
        webView.load(URLRequest(url: url))
    }
    
    func update(url: String?) {
        currentURL = url
    }
    
    @IBAction func backButtonPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
