//
//  LoginViewController.swift
//  itea_GM_HT_bucket_2.0
//
//  Created by Anatolii on 6/29/19.
//  Copyright © 2019 Anatolii. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {
    
    @IBOutlet weak var loginButton: UIButton!
    
    var companiesArray: [ITCompany] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loginButton.layer.cornerRadius = 10
        companiesArray = CompanyManager().createITCompany()
    }
    
    @IBAction func loginButtonPressed(_ sender: Any) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "MapViewController") as! MapViewController
        
        let ukraine = City()
        ukraine.longitude = 31.5172709
        ukraine.latitude =  49.978075
        ukraine.zoom = 5.23
        vc.update(currentCity: ukraine, filialsArray: companiesArray)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
}
