//
//  mapViewController.swift
//  itea_GM_HT_bucket_2.0
//
//  Created by Anatolii on 6/29/19.
//  Copyright © 2019 Anatolii. All rights reserved.
//

import UIKit
import GoogleMaps
import MessageUI

class MapViewController: UIViewController {
    
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var shadowView: UIView!
    @IBOutlet weak var infoView: UIView!
    @IBOutlet weak var companyImage: UIImageView!
    @IBOutlet weak var companyName: UILabel!
    @IBOutlet weak var emailButton: UIButton!
    @IBOutlet weak var telephoneButton: UIButton!
    @IBOutlet weak var globalButton: UIButton!
    
    
    var locationManager = CLLocationManager()
    var itCompaniesArray: [ITCompany] = []
    var companyTapped: ITCompany?
    var currentPosition: City?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mapView.delegate = self
        
        infoView.layer.cornerRadius = 15
        updatePosition(long: currentPosition?.longitude, lat: currentPosition?.latitude, locZoom: currentPosition?.zoom)
        updateMarkers()
        shadowViewVisibility(needToHide: true)
    }
    
    func update(currentCity: City?, filialsArray: [ITCompany]) {
        currentPosition = currentCity
        itCompaniesArray = filialsArray
    }
    
    func showAlert() {
        let alert = UIAlertController(title: "Action required", message: "Please choose info to show", preferredStyle: UIAlertController.Style.alert)
        self.present(alert, animated: true, completion: nil)
        
        alert.addAction(UIAlertAction(title: "company preview", style: .default, handler: { action in
            self.shadowViewVisibility(needToHide: false)
        }))
        
        alert.addAction(UIAlertAction(title: "company info", style: .cancel, handler: { action in
            self.showInfo()
        }))
    }
    
    func shadowViewVisibility(needToHide: Bool) {
        shadowView.isHidden = needToHide
        fillViewInfo(needToHide: needToHide)
    }
    
    func fillViewInfo(needToHide: Bool) {
        guard needToHide == false else {
            return
        }
        companyImage.image = UIImage(named: companyTapped?.logo ?? "default")
        companyName.text = companyTapped?.name
        
        emailButton.setTitle(companyTapped?.email, for: .normal)
        let underlineAttribute = [NSAttributedString.Key.underlineStyle: NSUnderlineStyle.single.rawValue]
        emailButton.titleLabel?.attributedText = NSAttributedString(string: companyTapped?.email ?? "e-mail", attributes: underlineAttribute)
        
        telephoneButton.setTitle(companyTapped?.telephone, for: .normal)
        telephoneButton.titleLabel?.attributedText = NSAttributedString(string: companyTapped?.telephone ?? "telephone", attributes: underlineAttribute)
    }
    
    func showInfo() {
        let vc = storyboard?.instantiateViewController(withIdentifier: "InfoViewController") as! InfoViewController
        vc.update(company: companyTapped)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func telephoneButtonPressed(_ sender: Any) {
        sendMessage(number: companyTapped?.telephone ?? "")
    }
    
    @IBAction func emailButtonPressed(_ sender: Any) {
        sendEmail(email: companyTapped?.email ?? "")
    }
    
    @IBAction func globalButtonPressed(_ sender: Any) {
        shadowViewVisibility(needToHide: true)
    }
    
}

extension MapViewController: CLLocationManagerDelegate, GMSMapViewDelegate {
    
    func updatePosition(long: Double?, lat: Double?, locZoom: Double?) {
        guard let _ = lat, let _ = long else {
            return
        }
        
        let camera = GMSCameraPosition.camera(withLatitude: lat ?? 0.0, longitude: long ?? 0.0, zoom: Float(locZoom ?? 11))
        self.mapView.animate(to: camera)
        self.locationManager.stopUpdatingLocation()
    }
    
    func updateMarkers() {
        
        for company in itCompaniesArray {
            let marker = GMSMarker()
            marker.position = CLLocationCoordinate2D(latitude: company.latitide, longitude: company.longitude)
            marker.icon = UIImage(named: "mark1")
            marker.title = company.name
            marker.userData = company
            //            marker.snippet = "working hours: \(museum.workingHours)"
            marker.map = mapView
        }
    }
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        companyTapped = marker.userData as? ITCompany
        showAlert()
        return false
    }
}

extension MapViewController: MFMessageComposeViewControllerDelegate {
    
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        controller.dismiss(animated: true, completion: nil)
    }
    
    
    func sendMessage(number: String) {
        let messageCompose = MFMessageComposeViewController()
        messageCompose.messageComposeDelegate = self
        messageCompose.recipients = [number]
        messageCompose.body = "Возьмите меня на работу пажжаааалуууста"
        messageCompose.subject = "Работаю за еду"
        present(messageCompose, animated: true, completion: nil)
    }
    
}

extension MapViewController: MFMailComposeViewControllerDelegate {
    
    func sendEmail(email: String) {
        let composeVC = MFMailComposeViewController()
        composeVC.mailComposeDelegate = self
        composeVC.setToRecipients([email])
        composeVC.setSubject("Hello! Wanna work hard!!")
        composeVC.setMessageBody("I want to work, please call me back!!", isHTML: false)
        present(composeVC, animated: true, completion: nil)
    }
    
}

